package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FetchTest {
    @Test
    void should_return_right_cars_when_fetch_given_parking_boy_with_two_parking_lots_parked_cars_and_tickets() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        Car car1 = new Car("1");
        Car car2 = new Car("2");
        ParkingBoy standardParkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2));

        ParkingTicket parkingTicket1 = standardParkingBoy.park(car1);
        ParkingTicket parkingTicket2 = standardParkingBoy.park(car2);

        //when
        Car fetchedCar1 = standardParkingBoy.fetch(parkingTicket1);
        Car fetchedCar2 = standardParkingBoy.fetch(parkingTicket2);

        //then
        assertEquals(car1, fetchedCar1);
        assertEquals(car2, fetchedCar2);
    }

    @Test
    void should_throw_unrecognized_exception_when_fetch_given_parking_boy_two_parking_lots_and_wrong_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        Car car1 = new Car("1");
        Car car2 = new Car("2");
        ParkingBoy standardParkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2));
        standardParkingBoy.park(car1);
        standardParkingBoy.park(car2);

        ParkingTicket wrongTicket = new ParkingTicket();

        //when
        //then
        UnrecognizedTicketException exception = assertThrows(UnrecognizedTicketException.class, () -> standardParkingBoy.fetch(wrongTicket));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());

    }

    @Test
    void should_throw_unrecognized_exception_when_fetch_given_parking_boy_two_parking_lots_and_used_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        Car car1 = new Car("1");
        ParkingBoy standardParkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2));
        ParkingTicket parkingTicket1 = standardParkingBoy.park(car1);
        standardParkingBoy.fetch(parkingTicket1);

        //when
        //then
        UnrecognizedTicketException exception = assertThrows(UnrecognizedTicketException.class, () -> standardParkingBoy.fetch(parkingTicket1));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());

    }



}
