package com.parkinglot;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {
    @Test
    void should_return_ticket_when_park_given_parking_lot_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car("1");

        //when
        ParkingTicket parkingTicket = parkingLot.park(car);

        //then
        assertNotNull(parkingTicket);
    }

    @Test
    void should_return_parked_car_when_fetch_given_parking_lot_with_parked_car_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car carToPark = new Car("1");
        ParkingTicket parkingTicket = parkingLot.park(carToPark);

        //when
        Car parkedCar = parkingLot.fetch(parkingTicket);

        //then
        assertEquals(carToPark, parkedCar);
    }

    @Test
    void should_return_right_car_when_fetch_given_parking_lot_with_two_parked_cars_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car("1");
        Car car2 = new Car("2");
        ParkingTicket parkingTicket1 = parkingLot.park(car1);
        ParkingTicket parkingTicket2 = parkingLot.park(car2);

        //when
        Car fetchedCar = parkingLot.fetch(parkingTicket2);

        //then
        assertEquals(car2, fetchedCar);
    }
    
    @Test
    void should_return_null_and_throw_unrec_exception_when_fetch_given_parking_lot_and_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car("1");
        Car car2 = new Car("2");
        parkingLot.park(car1);
        parkingLot.park(car2);
        ParkingTicket wrongTicket = new ParkingTicket();

        //when
        //then
        assertThrows(UnrecognizedTicketException.class, () -> {
            parkingLot.fetch(wrongTicket);
        });
    }

    @Test
    void should_return_null_and_throw_unrec_exception_when_fetch_given_parking_lot_and_used_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car("1");
        Car car2 = new Car("2");
        parkingLot.park(car1);
        ParkingTicket parkingTicket2 = parkingLot.park(car2);
        parkingLot.fetch(parkingTicket2);

        //when
        //then
        Throwable exception = assertThrows(UnrecognizedTicketException.class, () -> {
            parkingLot.fetch(parkingTicket2);
        });
        assertEquals("Unrecognized parking ticket.", exception.getMessage());

    }

    @Test
    void should_return_null_when_park_given_parking_lot_full() {
        //given
        ParkingLot limitedParkingLot = new ParkingLot(2);
        Car car1 = new Car("1");
        Car car2 = new Car("2");
        Car car3 = new Car("3");
        limitedParkingLot.park(car1);
        limitedParkingLot.park(car2);

        //when
        //then
        Throwable exception = assertThrows(NoAvailablePositionException.class, () -> {
            limitedParkingLot.park(car3);
        });
        assertEquals("No available position.", exception.getMessage());
    }

    


}
