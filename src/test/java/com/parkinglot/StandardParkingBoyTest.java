package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class StandardParkingBoyTest {
    @Test
    void should_return_ticket_when_park_given_parking_boy_two_parking_lots_and_one_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        Car car = new Car();
        ParkingBoy standardParkingBoy = new ParkingBoy();
        standardParkingBoy.addParkingLot(parkingLot1);
        standardParkingBoy.addParkingLot(parkingLot2);

        //when
        ParkingTicket parkingTicket = standardParkingBoy.park(car);

        //then
        assertEquals(car, parkingLot1.fetch(parkingTicket));

    }

    @Test
    void should_return_ticket_of_parked_car_of_second_lot_when_park_given_parking_boy_two_parking_lots_first_full_second_vacant_and_one_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot();
        Car car1 = new Car();
        Car car2 = new Car();

        ParkingBoy standardParkingBoy = new ParkingBoy();
        standardParkingBoy.addParkingLot(parkingLot1);
        standardParkingBoy.addParkingLot(parkingLot2);
        standardParkingBoy.park(car1);

        //when
        ParkingTicket parkingTicket2 = standardParkingBoy.park(car2);

        //then
        assertEquals(car2, parkingLot2.fetch(parkingTicket2));
    }

    @Test
    void should_throw_no_available_position_exception_when_park_given_parking_boy_car_two_full_parking_lots_() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        Car car1 = new Car("1");
        Car car2 = new Car("2");
        Car car3 = new Car("3");
        ParkingBoy standardParkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2));
        standardParkingBoy.park(car1);
        standardParkingBoy.park(car2);


        //when
        //then
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> standardParkingBoy.park(car3));
        assertEquals("No available position.", exception.getMessage());
    }



}
