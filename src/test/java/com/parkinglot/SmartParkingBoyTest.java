package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class SmartParkingBoyTest {
    @Test
    void should_return_ticket_of_lot2_when_park_given_smart_parking_boy_one_car_two_nonfull_lots_lot2_more_space() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(3);
        ParkingLot parkingLot2 = new ParkingLot(2);
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        parkingLot1.park(car1);
        parkingLot1.park(car2);

        ParkingBoy smartParkingBoy = new ParkingBoy(new SmartSearchStrategy(), List.of(parkingLot1, parkingLot2));

        //when
        ParkingTicket parkingTicket = smartParkingBoy.park(car3);

        //then
        assertEquals(car3, parkingLot2.fetch(parkingTicket));

    }

    @Test
    void should_return_ticket_of_lot1_when_park_given_smart_parking_boy_one_car_two_lots_same_num_empty_spaces() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(3);
        ParkingLot parkingLot2 = new ParkingLot(2);
        Car car1 = new Car();
        Car car2 = new Car();
        parkingLot1.park(car1);

        ParkingBoy smartParkingBoy = new ParkingBoy(new SmartSearchStrategy(), List.of(parkingLot1, parkingLot2));

        //when
        ParkingTicket parkingTicket = smartParkingBoy.park(car2);

        //then
        assertEquals(car2, parkingLot1.fetch(parkingTicket));

    }

    @Test
    void should_throw_no_available_position_when_park_given_smart_parking_boy_one_car_two_lots_full() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();

        ParkingBoy smartParkingBoy = new ParkingBoy(new SmartSearchStrategy(), List.of(parkingLot1, parkingLot2));
        smartParkingBoy.park(car1);
        smartParkingBoy.park(car2);

        //when
        //then
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> smartParkingBoy.park(car3));
        assertEquals("No available position.", exception.getMessage());

    }


}
