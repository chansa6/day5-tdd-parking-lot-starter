package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SuperParkingBoyTest {
    @Test
    void should_return_ticket_of_lot2_when_park_given_super_parking_boy_one_car_two_nonfull_lots_lot2_higher_vacant_rate() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(3); // vacant rate 1/3
        ParkingLot parkingLot2 = new ParkingLot(2); // vacant rate 1/2
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        Car carTarget = new Car();
        parkingLot1.park(car1);
        parkingLot1.park(car2);
        parkingLot2.park(car3);


        ParkingBoy superParkingBoy = new ParkingBoy(new SuperSearchStrategy(), List.of(parkingLot1, parkingLot2));

        //when
        ParkingTicket parkingTicket = superParkingBoy.park(carTarget);

        //then
        assertEquals(carTarget, parkingLot2.fetch(parkingTicket));

    }

    @Test
    void should_return_ticket_of_lot1_when_park_given_super_parking_boy_one_car_two_nonfull_lots_same_vacant_rate() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(2); // vacant rate 1/2
        ParkingLot parkingLot2 = new ParkingLot(2); // vacant rate 1/2
        Car car1 = new Car();
        Car car2 = new Car();
        Car carTarget = new Car();
        parkingLot1.park(car1);
        parkingLot2.park(car2);


        ParkingBoy superParkingBoy = new ParkingBoy(new SuperSearchStrategy(), List.of(parkingLot1, parkingLot2));

        //when
        ParkingTicket parkingTicket = superParkingBoy.park(carTarget);

        //then
        assertEquals(carTarget, parkingLot1.fetch(parkingTicket));

    }

    @Test
    void should_throw_no_available_position_when_park_given_super_parking_boy_one_car_two_full_lots_() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1); // vacant rate 1
        ParkingLot parkingLot2 = new ParkingLot(1); // vacant rate 1
        Car car1 = new Car();
        Car car2 = new Car();
        Car carTarget = new Car();
        parkingLot1.park(car1);
        parkingLot2.park(car2);


        ParkingBoy superParkingBoy = new ParkingBoy(new SuperSearchStrategy(), List.of(parkingLot1, parkingLot2));

        //when
        //then
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> superParkingBoy.park(carTarget));
        assertEquals("No available position.", exception.getMessage());

    }


}
