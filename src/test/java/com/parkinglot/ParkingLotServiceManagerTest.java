package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ParkingLotServiceManagerTest {

    @Test
    void should_return_ticket_of_lot1_when_park_with_boy_given_manager_two_managed_boys_both_vacant() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingLot parkingLot3 = new ParkingLot(1);
        Car car1 = new Car();

        ParkingBoy boy1 = new ParkingBoy(List.of(parkingLot1));
        ParkingBoy boy2 = new ParkingBoy(List.of(parkingLot2));
        ParkingLotServiceManager manager = new ParkingLotServiceManager(List.of(parkingLot3), List.of(boy1, boy2));


        //when
        ParkingTicket targetTicket = manager.parkWithManagedBoy(car1);

        //then
        assertEquals(car1, parkingLot1.fetch(targetTicket));

    }

    @Test
    void should_return_right_ticket_when_park_with_boy_given_manager_two_managed_boys_boy1_full_boy2_vacant() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingLot parkingLot3 = new ParkingLot(1);
        Car car1 = new Car();
        Car car2 = new Car();
        parkingLot1.park(car1);


        ParkingBoy boy1 = new ParkingBoy(List.of(parkingLot1));
        ParkingBoy boy2 = new ParkingBoy(List.of(parkingLot2));
        ParkingLotServiceManager manager = new ParkingLotServiceManager(List.of(parkingLot3), List.of(boy1, boy2));


        //when
        ParkingTicket targetTicket = manager.parkWithManagedBoy(car2);

        //then
        assertEquals(car2, parkingLot2.fetch(targetTicket));

    }

    @Test
    void should_throw_no_available_position_when_park_with_boy_given_manager_two_managed_boys_both_full() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingLot parkingLot3 = new ParkingLot(1);
        Car car1 = new Car();
        Car car2 = new Car();
        Car carTarget = new Car();
        parkingLot1.park(car1);
        parkingLot2.park(car2);


        ParkingBoy boy1 = new ParkingBoy(List.of(parkingLot1));
        ParkingBoy boy2 = new ParkingBoy(List.of(parkingLot2));
        ParkingLotServiceManager manager = new ParkingLotServiceManager(List.of(parkingLot3), List.of(boy1, boy2));


        //when
        //then
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> manager.parkWithManagedBoy(carTarget));
        assertEquals("No available position.", exception.getMessage());

    }


    @Test
    void should_return_right_car_from_boy_when_fetch_given_manager_two_managed_boys_one_with_right_car_right_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingLot parkingLot3 = new ParkingLot(1);
        Car car1 = new Car();
        Car carTarget = new Car();

        ParkingBoy boy1 = new ParkingBoy(List.of(parkingLot1));
        ParkingBoy boy2 = new ParkingBoy(List.of(parkingLot2));
        boy1.park(car1);
        ParkingTicket ticketTarget = boy2.park(carTarget);
        ParkingLotServiceManager manager = new ParkingLotServiceManager(List.of(parkingLot3), List.of(boy1, boy2));


        //when
        Car fetchedCar = manager.fetch(ticketTarget);

        //then
        assertEquals(carTarget, fetchedCar);

    }

    @Test
    void should_throw_unrecognized_ticket_when_fetch_given_manager_two_managed_boys_car_wrong_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingLot parkingLot3 = new ParkingLot(1);
        Car car1 = new Car();
        Car carTarget = new Car();

        ParkingBoy boy1 = new ParkingBoy(List.of(parkingLot1));
        ParkingBoy boy2 = new ParkingBoy(List.of(parkingLot2));
        boy1.park(car1);
        boy2.park(carTarget);
        ParkingLotServiceManager manager = new ParkingLotServiceManager(List.of(parkingLot3), List.of(boy1, boy2));
        ParkingTicket wrongTicket = new ParkingTicket();

        //when
        //then
        UnrecognizedTicketException exception = assertThrows(UnrecognizedTicketException.class, () -> manager.fetch(wrongTicket));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());

    }

    @Test
    void should_throw_unrecognized_ticket_when_fetch_with_boy_given_manager_two_managed_boys_car_expired_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingLot parkingLot3 = new ParkingLot(1);
        Car car1 = new Car();
        Car carTarget = new Car();

        ParkingBoy boy1 = new ParkingBoy(List.of(parkingLot1));
        ParkingBoy boy2 = new ParkingBoy(List.of(parkingLot2));
        boy1.park(car1);
        ParkingTicket ticketTarget = boy2.park(carTarget);
        ParkingLotServiceManager manager = new ParkingLotServiceManager(List.of(parkingLot3), List.of(boy1, boy2));

        manager.fetch(ticketTarget);

        //when
        //then
        UnrecognizedTicketException exception = assertThrows(UnrecognizedTicketException.class, () -> manager.fetch(ticketTarget));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());

    }

    @Test
    void should_return_right_car_from_manager_when_fetch_given_manager_with_parked_car_two_managed_boys_car_right_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingLot parkingLot3 = new ParkingLot(1);
        Car car1 = new Car();
        Car carTarget = new Car();

        ParkingBoy boy1 = new ParkingBoy(List.of(parkingLot1));
        ParkingBoy boy2 = new ParkingBoy(List.of(parkingLot2));
        boy1.park(car1);
        ParkingLotServiceManager manager = new ParkingLotServiceManager(List.of(parkingLot3), List.of(boy1, boy2));
        ParkingTicket ticketTarget = manager.park(carTarget);

        //when
        Car fetchedCar = manager.fetch(ticketTarget);

        //then
        assertEquals(carTarget, fetchedCar);

    }

    @Test
    void should_park_lot3_when_park_with_manager_given_manager_two_managed_boys_all_three_vacant() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingLot parkingLot3 = new ParkingLot(1);
        Car carTarget = new Car();

        ParkingBoy boy1 = new ParkingBoy(List.of(parkingLot1));
        ParkingBoy boy2 = new ParkingBoy(List.of(parkingLot2));
        ParkingLotServiceManager manager = new ParkingLotServiceManager(List.of(parkingLot3), List.of(boy1, boy2));


        //when
        ParkingTicket targetTicket = manager.park(carTarget);

        //then
        assertEquals(carTarget, parkingLot3.fetch(targetTicket));
    }

    @Test
    void should_park_lot3_when_park_with_manager_given_manager_two_managed_boys_all_three_vacant_manager_has_two_vacant_lots() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingLot parkingLot3 = new ParkingLot(1);
        ParkingLot parkingLot4 = new ParkingLot(2);
        Car carTarget = new Car();

        ParkingBoy boy1 = new ParkingBoy(List.of(parkingLot1));
        ParkingBoy boy2 = new ParkingBoy(List.of(parkingLot2));
        ParkingLotServiceManager manager = new ParkingLotServiceManager(List.of(parkingLot3, parkingLot4), List.of(boy1, boy2));


        //when
        ParkingTicket targetTicket = manager.park(carTarget);

        //then
        assertEquals(carTarget, parkingLot3.fetch(targetTicket));
    }

    @Test
    void should_throw_no_available_position_when_park_with_manager_given_manager_two_managed_boys_both_vacant_manager_has_two_full_lots() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingLot parkingLot3 = new ParkingLot(0);
        ParkingLot parkingLot4 = new ParkingLot(0);
        Car carTarget = new Car();

        ParkingBoy boy1 = new ParkingBoy(List.of(parkingLot1));
        ParkingBoy boy2 = new ParkingBoy(List.of(parkingLot2));
        ParkingLotServiceManager manager = new ParkingLotServiceManager(List.of(parkingLot3, parkingLot4), List.of(boy1, boy2));


        //when
        //then
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> manager.park(carTarget));
        assertEquals("No available position.", exception.getMessage());
    }



}
