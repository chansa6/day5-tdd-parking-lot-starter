package com.parkinglot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ParkingBoy {
    private List<ParkingLot> parkingLots = new ArrayList<>();
    private final HashMap<ParkingTicket, ParkingLot> ticketToLot = new HashMap<>();
    private final IGreedySearchStrategy greedySearchStrategy;

    public ParkingBoy() {
        this.greedySearchStrategy = new StandardSearchStrategy();
    }

    public ParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
        this.greedySearchStrategy = new StandardSearchStrategy();
    }

    public ParkingBoy(IGreedySearchStrategy greedySearchStrategy, List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
        this.greedySearchStrategy = greedySearchStrategy;
    }


    public ParkingTicket park(Car car) {
        ParkingLot optimalParkingLot = this.greedySearchStrategy.getOptimalParkingLot(this.parkingLots);
        if (optimalParkingLot == null) {
            throw new NoAvailablePositionException();
        }
        ParkingTicket generatedTicket = optimalParkingLot.park(car);
        ticketToLot.put(generatedTicket, optimalParkingLot);
        return generatedTicket;
    }


    public Car fetch(ParkingTicket parkingTicket) {
        if (!ticketToLot.containsKey(parkingTicket)) {
            throw new UnrecognizedTicketException();
        }
        return ticketToLot.get(parkingTicket).fetch(parkingTicket);
    }

    public void addParkingLot(ParkingLot parkingLot) {
        this.parkingLots.add(parkingLot);
    }

    public boolean hasAvailableParkingLot() {
        return parkingLots.stream().anyMatch(ParkingLot::isVacant);
    }

    public boolean hasParkedCarFromTicket(ParkingTicket parkingTicket) {
        return ticketToLot.containsKey(parkingTicket);
    }

}
