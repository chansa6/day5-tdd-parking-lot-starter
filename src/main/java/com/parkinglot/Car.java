package com.parkinglot;

public class Car {
    private String carID;

    public Car(String carID) {
        this.carID = carID;
    }

    public Car() {

    }

    public String getCarID() {
        return this.carID;
    }
}
