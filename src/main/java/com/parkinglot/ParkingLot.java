package com.parkinglot;

import java.util.*;

public class ParkingLot {

    private final HashMap<ParkingTicket, Car> parkedCars;

    private int parkingCapacity = 10;

    public ParkingLot() {
        this.parkedCars = new HashMap<>();
    }

    public ParkingLot(int newParkingCapacity) {
        this.parkedCars = new HashMap<>();
        this.parkingCapacity = newParkingCapacity;
    }


    public ParkingTicket park(Car car) {
        if (parkedCars.size() >= parkingCapacity) {
            throw new NoAvailablePositionException();
        }
        ParkingTicket parkingTicket = new ParkingTicket();
        parkedCars.put(parkingTicket, car);
        return parkingTicket;
    }

    public Car fetch(ParkingTicket parkingTicket) {
        if (this.parkedCars.containsKey(parkingTicket)) {
            return parkedCars.remove(parkingTicket);
        }
        throw new UnrecognizedTicketException();

    }

    public int getParkingCapacity() {
        return parkingCapacity;
    }

    public boolean isVacant() {
        return this.parkedCars.size() < this.parkingCapacity;
    }

    public int getVacancy() {
        return this.parkingCapacity - this.parkedCars.size();
    }


}
