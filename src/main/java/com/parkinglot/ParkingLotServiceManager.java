package com.parkinglot;

import java.util.List;

public class ParkingLotServiceManager extends ParkingBoy{
    private final List<ParkingBoy> managedParkingBoys;

    public ParkingLotServiceManager(List<ParkingLot> parkingLots, List<ParkingBoy> managedParkingBoys) {
        super(parkingLots);
        this.managedParkingBoys = managedParkingBoys;
    }

    private ParkingBoy getFirstAvailableManagedParkingBoy() {
        return this.managedParkingBoys.stream().filter(ParkingBoy::hasAvailableParkingLot).
                findFirst().orElse(null);
    }

    private ParkingBoy getParkingBoyWithTicket(ParkingTicket parkingTicket) {
        return this.managedParkingBoys.stream().
                filter(boy -> boy.hasParkedCarFromTicket(parkingTicket))
                .findFirst().orElse(null);
    }

    public ParkingTicket parkWithManagedBoy(Car car) {
        ParkingBoy chosenParkingBoy = this.getFirstAvailableManagedParkingBoy();
        if (chosenParkingBoy == null) throw new NoAvailablePositionException();
        return chosenParkingBoy.park(car);
    }

    @Override
    public Car fetch(ParkingTicket parkingTicket) {
        ParkingBoy chosenParkingBoy = this.getParkingBoyWithTicket(parkingTicket);
        return chosenParkingBoy != null ? chosenParkingBoy.fetch(parkingTicket) : super.fetch(parkingTicket);
    }


}
