package com.parkinglot;

import java.util.Comparator;
import java.util.List;

public class SuperSearchStrategy implements IGreedySearchStrategy{

    @Override
    public ParkingLot getOptimalParkingLot(List<ParkingLot> parkingLots) {
        return parkingLots.stream()
                .max(Comparator.comparingDouble(lot
                        -> (double) lot.getVacancy() / lot.getParkingCapacity()))
                            .orElse(null);
    }
}
