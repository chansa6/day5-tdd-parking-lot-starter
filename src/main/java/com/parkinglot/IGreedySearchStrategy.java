package com.parkinglot;

import java.util.List;

public interface IGreedySearchStrategy {
    ParkingLot getOptimalParkingLot(List<ParkingLot> parkingLots);
}
