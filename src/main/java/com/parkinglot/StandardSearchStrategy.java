package com.parkinglot;

import java.util.List;

public class StandardSearchStrategy implements IGreedySearchStrategy {
    @Override
    public ParkingLot getOptimalParkingLot(List<ParkingLot> parkingLots) {
        return parkingLots.stream().filter(ParkingLot::isVacant).
                findFirst().orElse(null);
    }
}
