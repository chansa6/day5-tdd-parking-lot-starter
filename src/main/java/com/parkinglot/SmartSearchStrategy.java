package com.parkinglot;

import java.util.Comparator;
import java.util.List;

public class SmartSearchStrategy implements IGreedySearchStrategy{
    @Override
    public ParkingLot getOptimalParkingLot(List<ParkingLot> parkingLots) {
        return parkingLots.stream()
                .max(Comparator.comparingInt(ParkingLot::getVacancy))
                .orElse(null);
    }
}
